# -*- coding: utf-8 -*-

from selenium import webdriver
import sys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
import time

opt = webdriver.ChromeOptions()
opt.add_experimental_option('w3c', False)
driver = webdriver.Chrome(chrome_options=opt)
# browser.get('https://ssl.biglion.ru/checkout/order/puteshestvie-navozdushnom-share1-51/5144017/')
driver.get(sys.argv[1])

actions = ActionChains(driver)
wait = WebDriverWait(driver, 10)
wait5 = WebDriverWait(driver, 5)
wait15 = WebDriverWait(driver, 15)

link = wait.until(EC.visibility_of_element_located((By.XPATH, '//div[@class="button_block "]')))
link.click()
time.sleep(5)
driver.get("https://www.bethowen.ru/basket/")
link = wait.until(EC.visibility_of_element_located((By.XPATH, '//button[@data-entity="basket-checkout-button"]')))
link.click()
time.sleep(15)

valid_promocodes = open("bethowen_valid_promocodes.txt",'w')
invalid_promocodes = open("bethowen_invalid_promocodes.txt",'w')
promocodes = [line.strip() for line in open('bethowen_promocodes.txt', "r")]		
count = 0
for promocode in promocodes:
	count = count + 1
	print("processing promocode %s (%s from %s)" % (promocode, count, len(promocodes)))
	editor = wait.until(EC.visibility_of_element_located((By.XPATH, '//div[@class="bx-soa-coupon-input"]/input'))) 
	editor.send_keys(promocode)
	time.sleep(5)
	location = editor.location
	size = editor.size
	actions.move_to_element_with_offset(driver.find_element_by_tag_name('body'), 0,0)
	actions.move_by_offset(location['x'] + size["width"] + 10, location['y'] + 10).click().perform()	
	
	try:
		element = wait15.until(EC.visibility_of_element_located((By.XPATH, '//strong[@class="bx-soa-coupon-item-success"]')))
		print("promocode %s is VALID" % promocode)
		valid_promocodes.write(promocode)
		valid_promocodes.write("\n")
		link = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[@class="bx-soa-coupon-remove"]')))
		link.click()
		time.sleep(12)
	except Exception, e:
		print("promocode %s is INVALID" % promocode)
		invalid_promocodes.write(promocode)
		invalid_promocodes.write("\n")
		link = wait.until(EC.visibility_of_element_located((By.XPATH, '//span[@class="bx-soa-coupon-remove"]')))
		link.click()
		time.sleep(5)
	time.sleep(3)
 
valid_promocodes.close()
invalid_promocodes.close()			
print("all completed")
driver.quit()